#!/bin/bash

# This script detects the Linux OS and configures it accordingly.
## Currently supports Debian, Fedora, EndeavourOS, and Ubuntu.

## Set variables and arrays
### Variables
declare clone_path="/home/${USER}/Downloads/envycontrol"

### Packages and repos
flatpaks=("com.github.tchx84.Flatseal" "com.google.Chrome" "com.mattjakeman.ExtensionManager" "com.slack.Slack" "net.supertuxkart.SuperTuxKart" "org.gnome.Boxes" "org.gnome.Builder" "org.gnome.PowerStats" "com.mattjakeman.ExtensionManager" "org.libreoffice.LibreOffice" "org.mozilla.firefox" "org.signal.Signal" "sh.cider.Cider" "us.zoom.Zoom" "net.cozic.joplin_desktop" "com.belmoussaoui.Obfuscate")
pack_arch=(vim git htop tmux papirus-icon-theme flatpak debtap timeshift nfs-utils ruby rsync tldr glances neofetch gnome-tweaks chromium docker kubernetes minikube gnome-software-plugin-flatpak ncdu ttf-fira-mono ttf-fira-sans archlinux-wallpaper)
pack_deb=(sudo vim git htop tmux rsync wget nala ruby tealdeer curl neofetch gnome-tweaks papirus-icon-theme gnome-software-plugin-flatpak docker-ce docker-ce-cli containerd.io docker-compose-plugin bash-completion arch-install-scripts speedtest-cli ncdu ttf-mscorefonts-installer rar unrar libavcodec-extra gstreamer1.0-libav gstreamer1.0-plugins-ugly gstreamer1.0-vaapi ccze)
pack_fed=(vim git htop tmux papirus-icon-theme timeshift nfs-utils ruby rsync tldr glances neofetch gnome-tweaks chromium google-noto-emoji-color-fonts helm kubernetes gnome-software-plugin-flatpak ncdu)
pack_ubuntu=(vim git htop tmux rsync wget ruby tldr neofetch gnome-tweaks papirus-icon-theme flatpak gnome-software-plugin-flatpak arch-install-scripts ncdu vanilla-gnome-desktop speedtest-cli curl)
deb_repos=(contrib non-free non-free-firmware)
snap_rm=(snap-store gtk-common-themes gnome-3-38-2004 gnome-42-2204 core20 core22 snapd-desktop-integration bare snapd) ## This order matters

## Functions 
function docker_debian {
  sudo mkdir -p /etc/apt/keyrings
  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo apt update
  sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
}

### Detect user, reject root user 
if (( $EUID == 1 )); then
    echo "It is not advised to run this as root"
    exit 1
fi

echo "####################################################################"
echo "#This script is interactive and will prompt for your sudo password.#"
echo "####################################################################"
sleep 3

## Detect OS and do stuff accordingly

### Debian
if [[ $(cat /etc/os-release) =~ 'ID=debian' ]]; then
    echo "Found Debian."
    for repo in "${deb_repos[@]}"; do
      sudo apt-add-repository "$repo" -y;
    done
    sudo apt update && sudo apt upgrade -y
    sudo apt install -y -m "${pack_deb[@]}"
    docker_debian

### EndeavourOS
elif [[ $(cat /etc/os-release) =~ 'ID=endeavouros' ]]; then
    echo "Found EndeavourOS."
    yay -Syy && yay -S "${pack_arch[@]}"

### Fedora
elif [[ $(cat /etc/os-release) =~ 'NAME="Fedora Linux"' ]]; then 
    echo "Found Fedora."
    flatpak remote-delete fedora
    sudo dnf install "${pack_fed[@]}" --skip-broken
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
    sudo dnf install docker-ce docker-ce-cli containerd.io docker-compose-plugin

### Ubuntu
elif [[ $(cat /etc/os-release) =~ 'ID=ubuntu' ]]; then
    echo "Found Ubuntu."
    sudo apt update && sudo apt upgrade -y 
    sudo apt install -y "${pack_ubuntu[@]}"
    docker_debian
    for snap in $(snap list | cut -f1 -d' ' | egrep -v 'Name|bare|core|gnome|snap'); do
      sudo snap remove $snap;
    done
    for snap in "${snap_rm[@]}"; do sudo snap remove "$snap"; done
    rm -rf /home/$USER/snap && sudo rm -rf /snap /var/snap /var/lib/snapd
    sudo apt purge -y snapd gnome-software-plugin-snap && sudo apt-mark hold snapd
    sudo dpkg-reconfigure unattended-upgrades

### Exit for other OSs
else
  echo "This script did not detect a supported Linux operating system."
  exit 1
fi

## Disable NVIDIA graphics. TODO: Meet condition of Nvidia _and_ Intel graphics
if [[ $(sudo lspci -v | grep NVIDIA) ]]; then
	echo "NVIDIA GPU found. Cloning envycontrol into ${clone_path}."
	git clone https://github.com/bayasdev/envycontrol.git "${clone_path}"
	echo "Disabling discrete GPU for power and thermal management."
	sudo python3 "${clone_path}/envycontrol.py" -s integrated
else
	echo "No NVIDIA graphics found."
fi

## Install Flatpaks
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    for pak in "${flatpaks[@]}"; do
      flatpak install --noninteractive $pak
    done
echo 'Restart for flatpaks to appear in shell.' 

## Add user to Docker group; enable and start Docker daemon
sudo usermod -aG docker "$USER" && sudo systemctl enable --now docker

## Fetch and install minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

## Add aliases
echo "alias kubectl='minikube kubectl --'" >> /home/"$USER"/.bashrc
echo "alias docs-up='docker run -it --rm -d -p 4000:4000 registry.gitlab.com/gitlab-org/gitlab-docs:15.0 ; docker run -it --rm -d -p 4001:4000 docs/docker.github.io'" >> /home/"$USER"/.bashrc

## FF env variable for Wayland goodies
echo "MOZ_ENABLE_WAYLAND=1" | sudo tee /etc/environment.d/moz >/dev/null
